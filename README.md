# Kubernetes on Cloud

Deploy K8s on AWS

# Deploy EC2 virtual machines with Terraform

## Configure your aws local user configuration

You can create a specific AWS user on AWS Web Console with full admin privileges
and use this user (i.e. the corresponding access key and secret access key) later
on. Then you could delete this user at the end of this hands on exercices.

Create your `.aws` personal configuration directory where you will put two files.

`~/.aws/credentials`

```
[default]
aws_access_key_id=YYRR(YOUR ACCESS KEY)UUU
aws_secret_access_key=887AA(YOUR SECRET ACCESS KEY)UUY667
``` 

Make sure only YOU can read that file: `chmod a=rw,go= ~/.aws/credentials`

`~/.aws/config`

```
[default]
region=us-east-2c
output=json
```

We are suggesting us-east-2c as the AMI we will be using is only available there, anyway
it is very likely than any reasonable Debian 9/10 AMI, or a recent CentOS will do (you
may have to adapt Terraform files in this case)

In addition you do have to accept terms and subcribe to the AMI you intend to use
in AWS Web Console (ami-0788cc5a8c3155f0f if you do not change instance.tf).

## Install and run Terraform

Install terraform (last version, or v0.12.18 this procedure has been tested with) and
put terraform binary in a directory which is in your PATH.

In addition we need jq which is used by the bastion login script.

Get the files from this repository, build files and initialize Terraform, then construct
your EC2 infrastructure:

```
sudo apt install jq
git clone https://framagit.org/jpython/k8s-on-cloud.git
cd k8s-on-cloud/
cd ssh-keys/
./generate_keys 
cd ../Scripts/
make
cd ../Tf/
terraform init
terraform plan
terraform apply
```

If every went well you should be able to connect to your bastion ec2 instance:

```
$ ./go_bastion
Bastion public ip: 3.15.139.236
Warning: Permanently added '3.15.139.236' (ECDSA) to the list of known hosts.
Linux ip-10-0-0-42 4.9.0-11-amd64 #1 SMP Debian 4.9.189-3 (2019-09-02) x86_64
...
admin@ip-10-0-0-42:~$ 
```

You first have to wait for cloud-init to have finished, at that time a
file `/tmp/cloud-init-bastion-ok` will appear. If you get bored you can
run this waiting for the cloud-init process to disappear :

```
admin@ip-10-0-0-42:~$ watch pstree
```

Then you'll have to wait (again!) for all nodes to be properly setted up:

```
admin@ip-10-0-0-42:~$ watch ./check_node_ci 50 100 101 102 
```

When you'll see a file `/tmp/cloud-init-node-ok` at all nodes you can move forward.

# Deploy Kubernetes with an Ansible Playbook

Still logged in your bastion virtual machine, clone another repository which
contains the playbook and run it:

```
admin@ip-10-0-0-42:~$ git clone https://framagit.org/jpython/kubeadm-ansible.git
admin@ip-10-0-0-42:~$ cd kubeadm-ansible/
admin@ip-10-0-0-42:~/kubeadm-ansible$ ansible-playbook site.yaml 
```

In some cases a given node may fail to be configured (network glith on AWS
side?). You can *taint* it with Terraform `terraform tain resource_name` then
apply terraform again to destroy and recreate it.

Check that Kubernetes cluster is up and running:

```
admin@ip-10-0-0-42:~/kubeadm-ansible$ ssh root@10.0.0.50
root@ip-10-0-0-50:~# kubectl get nodes
NAME            STATUS   ROLES    AGE   VERSION
ip-10-0-0-100   Ready    <none>   33m   v1.17.0
ip-10-0-0-101   Ready    <none>   33m   v1.17.0
ip-10-0-0-102   Ready    <none>   33m   v1.17.0
ip-10-0-0-50    Ready    master   33m   v1.17.0

root@ip-10-0-0-50:~# kubectl get pods -n kube-system
NAME                                      READY   STATUS    RESTARTS   AGE
calico-kube-controllers-55754f75c-wvkbw   1/1     Running   0          3h37m
calico-node-4sczw                         1/1     Running   0          3h37m
calico-node-75xbj                         1/1     Running   0          3h37m
calico-node-hqngl                         1/1     Running   0          3h37m
calico-node-mz7v5                         1/1     Running   0          3h37m
coredns-6955765f44-knvfh                  1/1     Running   0          3h37m
coredns-6955765f44-pxj5p                  1/1     Running   0          3h37m
etcd-ip-10-0-0-50                         1/1     Running   0          3h38m
kube-apiserver-ip-10-0-0-50               1/1     Running   0          3h38m
kube-controller-manager-ip-10-0-0-50      1/1     Running   0          3h38m
kube-proxy-fj97j                          1/1     Running   0          3h37m
kube-proxy-jj8nw                          1/1     Running   0          3h37m
kube-proxy-mcn5r                          1/1     Running   0          3h37m
kube-proxy-xdhwh                          1/1     Running   0          3h37m
kube-scheduler-ip-10-0-0-50               1/1     Running   0          3h38m
```

## Add a new worker node

- Modify instances.tf to add a new worker virtual machine
- Reapply Terraform configuration, wait for the new vm to finish cloud-init process
- Log to the bastion and update hosts.ini 
- Ask Ansible to set up the new node: `ansible-playbook site.yaml`
- Check with kubectl that the new node is up and running

Files to modify in order to add new workers :

- `instances.tf` on your local machine (then Terraform has to be re-applied)
- `hosts.ini` on the bastion Ansible playbook repository

You may want to examine `group_vars/all.yml` in the same repository, this is
the Kubernetes cluster configuration.

The playbook can be replayed by using the same command you've ran initially
(`ansible-playbook site.yaml`), the whole cluster can be resetted by runing 
 `ansible-playbook reset-site.yaml`.

# Deployement

You can test a very simple deployment with helm as root on the master node:

```
curl https://raw.githubusercontent.com/kubernetes/helm/master/scripts/get-helm-3 > get_helm.sh
chmod +x get_helm.sh 
./get_helm.sh 
helm repo add stable https://kubernetes-charts.storage.googleapis.com
helm repo add bitnami https://charts.bitnami.com/bitnami
helm repo update
helm search repo bitnami | grep nginx
helm install bitnami/nginx --generate-name 
```

Wait for the pod to start and look for the corresponding IP for the service:

```
# kubectl get pods
NAME                                READY   STATUS    RESTARTS   AGE
nginx-1578899914-7469d55755-vzjd2   1/1     Running   0          54m

# kubectl get svc
NAME               TYPE           CLUSTER-IP     EXTERNAL-IP   PORT(S)                      AGE
kubernetes         ClusterIP      10.96.0.1      <none>        443/TCP                      61m
nginx-1578899914   LoadBalancer   10.96.X.Y   <pending>     80:31825/TCP,443:30301/TCP   54m
```

You can test that the nginx default home page is there:

```
# apt install w3m
# w3m http://10.96.X.Y
```

type 'q' to quit w3m, a very convenient console based Web navigator.

You can access your service from anywhere without bothering to get an elastic IP.

- Update security group for the bastion to allow 80/TCP and 443/TCP traffic from anywhere,
reapply terraform
- Log on the bastion and configure SSH tunnels, replace 10.96.X.Y with the service IP
you've got above:

```
sudo ssh -L *:80:10.96.X.Y:80 10.0.0.50 -f -N 
sudo ssh -L *:443:10.96.X.Y:443 10.0.0.50 -f -N 
```

You can then access the nginx home page from anywhere at url http://bastionIP/.

To stop such SSH tunnels just pick the ssh client process -`ps aux | grep ssh` and kill it.

You can subscribe to a free dynamic DNS service such as noip.com to get a
fancy name such as `gudule.zapto.org`.

## Deployement without storage provider

You can follow this documentation in order to deploy a basic Wordpress site:
https://kubernetes.io/docs/tutorials/stateful-application/mysql-wordpress-persistent-volume/

If you do not have a storage provider just remove all sections about storage, claims
and volume mounts in yaml files. It works out of the box.

## Add a storage provider (optional)

## Deploy apache/wordpress/mariadb in the cluster

## Deploy several apache/wordpress and add a load balancer

## Check and document previous steps

## How to add a new worker and increase the number of apache running?

